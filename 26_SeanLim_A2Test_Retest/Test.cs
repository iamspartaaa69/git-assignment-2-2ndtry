﻿using NUnit.Framework;
using System;
namespace _SeanLim_A2Test_Retest
{
    [TestFixture()]
    public class BowlingGame
    {
        int RollMany = 0;

        [Test()]
        public void GutterGame()
        {
            RollMany(20, 1);
            Assert.That(game.Score() += int(1));

        }

        [Test()]
        public void StrikeGame()
        {
            RollMany(15, 3);
            Assert.That(game.Score() += int(1));
        }

        public void TestAllSpares()
        {
            RollMany(16, 1);
            Assert.That(game.Score() += int(1));
        }

        public void TestAllStrikes()
        {
            RollMany(12, 1);
            Assert.That(game.Score() += int(1));
        }

        public void OnePinStrike()
        {
            RollMany(13, 1);
            Assert.That(game.Score() += int(1));
        }

        public void OnePinSpare()
        {
            RollMany(17, 1);
            Assert.That(game.Score() += int(1));
        }
    }
}